//import com.sun.tools.javac.util.Pair;

public class RuleEngine {
//    public Pair<String, String> = new Pair<String, String>;

    public static final int COOPERATE_COOPERATE_SCORE = 2;
    public static final int COOPERATE_CHEAT_SCORE = -1;
    public static final int CHEAT_COOPERATE_SCORE = 3;
    public static final int CHEAT_CHEAT_SCORE = 0;


    public Score scoreForTheMove(moveType player1Move, moveType player2Move) {
        if (player1Move == moveType.COOPERATE && player2Move == moveType.COOPERATE){
            return new Score(COOPERATE_COOPERATE_SCORE,COOPERATE_COOPERATE_SCORE);
        }else if(player1Move.equals(moveType.CHEAT) && player2Move.equals(moveType.COOPERATE)){
            return new Score(CHEAT_COOPERATE_SCORE,COOPERATE_CHEAT_SCORE);
        }else if(player1Move.equals(moveType.COOPERATE) && player2Move.equals(moveType.CHEAT)) {
            return new Score(COOPERATE_CHEAT_SCORE, CHEAT_COOPERATE_SCORE);
        } else {
            return new Score(CHEAT_CHEAT_SCORE, CHEAT_CHEAT_SCORE);
        }
    }
}
