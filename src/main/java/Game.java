public class Game {

    private final Player playerOne;
    private final Player playerTwo;
    private RuleEngine ruleEngine;
    private int player1Score;
    private int player2Score;


    public Game(Player playerOne, Player playerTwo, RuleEngine ruleEngine) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.ruleEngine = ruleEngine;
    }

    public Score nextScore() {
        return ruleEngine.scoreForTheMove(playerOne.getMove(), playerTwo.getMove());
    }

    public Score nextScoreAfterNRound(int n) {
        Score score = new Score(0,0);
        Score currentScore = this.nextScore();
        for (int i=0;i<n;i++){
            score.updateScore(currentScore);
            System.out.println("Round: "+(i+1)+" || "+score.toString());
        }
        return score;
    }
}
