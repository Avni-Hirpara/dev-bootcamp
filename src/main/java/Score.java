public class Score {
    private  int playerOne;
    private  int playerTwo;

    public int getPlayerOne() {
        return playerOne;
    }

    public int getPlayerTwo() {
        return playerTwo;
    }
    public void updateScore(Score score){
        this.playerOne += score.getPlayerOne();
        this.playerTwo += score.getPlayerTwo();
    }

    public Score(int playerOne, int playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
    }

    @Override
    public String toString() {
        return "Player1 Score :"+this.playerOne+ " || Player2 Score: "+ this.playerTwo;
    }
}
