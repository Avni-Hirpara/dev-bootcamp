import org.junit.Test;

import java.sql.SQLOutput;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class GameTest {
    private RuleEngine ruleEngine = new RuleEngine();
    @Test
    public void shouldReturnScoreForBothCooperate() {
        Player coolPlayerOne = new Player(moveType.COOPERATE);
        Player coolPlayerTwo = new Player(moveType.COOPERATE);
        Game game = new Game(coolPlayerOne, coolPlayerTwo, ruleEngine);
        Score score = game.nextScore();
        assertEquals(2, score.getPlayerOne());
        assertEquals(2, score.getPlayerTwo());
    }

    @Test
    public void shouldReturnScoreForBothCooperate5times() {
        Player coolPlayerOne = new Player(moveType.COOPERATE);
        Player coolPlayerTwo = new Player(moveType.COOPERATE);
        Game game = new Game(coolPlayerOne, coolPlayerTwo, ruleEngine);
        Score score = game.nextScoreAfterNRound(5);
        assertEquals(10, score.getPlayerOne());
        assertEquals(10, score.getPlayerTwo());
    }


    @Test
    public void shouldReturnScoreForCheatBothCooperate5times() {
        Player coolPlayerOne = new Player(moveType.COOPERATE);
        Player miserPlayer = new Player(moveType.CHEAT);
        Game game = new Game(coolPlayerOne, miserPlayer, ruleEngine);
        Score score = game.nextScoreAfterNRound(5);
        assertEquals(-5, score.getPlayerOne());
        assertEquals(15, score.getPlayerTwo());
    }

}