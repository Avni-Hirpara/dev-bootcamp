import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class RuleEngineTest {
    RuleEngine eot = new RuleEngine();
    @Test
    public void shouldReturnIfBothCorporate() {
        Score score = eot.scoreForTheMove(moveType.COOPERATE, moveType.COOPERATE);
        assertEquals(2, score.getPlayerOne());
        assertEquals(2, score.getPlayerTwo());
    }

    @Test
    public void shouldReturnIfBothCheat() {
        Score score = eot.scoreForTheMove(moveType.CHEAT, moveType.CHEAT);
        assertEquals(0, score.getPlayerOne());
        assertEquals(0, score.getPlayerTwo());
    }

    @Test
    public void shouldReturnIfPlayer1Cheat() {
        Score score = eot.scoreForTheMove(moveType.CHEAT, moveType.COOPERATE);
        assertEquals(3, score.getPlayerOne());
        assertEquals(-1, score.getPlayerTwo());
    }

    @Test
    public void shouldReturnIfPlayer2Cheat() {
        Score score = eot.scoreForTheMove(moveType.COOPERATE, moveType.CHEAT);
        assertEquals(-1, score.getPlayerOne());
        assertEquals(3, score.getPlayerTwo());
    }

}

